var alphabet = "abcdefghijklmnopqrstuvwxyz"
var nombres = "0123456789"
var num_to_char = nombres + alphabet

function prefix(base) {
    switch (base) {
        case 16:
            return "0x"
        case 10:
            return ""
        case 8:
            return "0o"
        case 2:
            return "0b"
        default:
            console.log(`unknown base: ${base}`)
            break;
    }
}
function tobigint(num_str, base) {
    num_prefix = prefix(base)
    return BigInt(`${num_prefix}${num_str}`)
}

function getvalofid(id) {
    return document.getElementById(id).value
}
function setoutput(text) {
    document.getElementById("resultat").innerText = text
    return 0
}

function pad(nbr_str, nbr_base, taillemot) {
    if (nbr_base == 10) {
        return nbr_str
    } else if (nbr_base == 2) {
        return nbr_str.padStart(taillemot, 0)
    } else if (nbr_base == 8) {
        return nbr_str.padStart(taillemot / 3, 0)
    } else if (nbr_base == 16) {
        return nbr_str.padStart(taillemot / 4, 0)
    }
}

function new_output() {
    var nombre = getvalofid("lenombre")
    var nombre_sign_cb = document.getElementById("lenombresign")
    var nombre_sign = nombre_sign_cb.checked
    var baseorigine = parseInt(getvalofid("labaseorigine"))
    var basecible = parseInt(getvalofid("labasecible"))
    var negative = undefined
    if (basecible != 10 && baseorigine != 10) {
        nombre_sign_cb.checked = false
        nombre_sign_cb.disabled = true
    } else if (nombre.charAt(0) == "-" && baseorigine == 10) {
        nombre = nombre.substr(1)
        nombre_sign_cb.disabled = true
        nombre_sign_cb.checked = true
        negative = true
    } else {
        nombre_sign_cb.disabled = false
    }
    var taillemot = parseInt(getvalofid("taillemot"))
    if (nombre == NaN || nombre == "") {
        return ""
    }
    var decimal = tobigint(nombre, baseorigine)
    var binary = pad(decimal.toString(2), 2, taillemot)
    if (nombre_sign && (binary.charAt(0) == "1")) {
        decimal = -(2n ** BigInt(taillemot - 1) - BigInt(`0b${binary.substr(1)}`))
    } else if (negative) {
        decimal = 2n ** BigInt(taillemot) - BigInt(`0b${binary}`)
    }
    var output = decimal.toString(basecible)
    if (output.charAt(0) == "-" && basecible != 10) {
        output = output.substr(1)
    }
    return pad(output, basecible, taillemot)
}

function onmodify() {
    try {
        setoutput(new_output())
    } catch (error) {
        setoutput("Entrée invalide")
    }
}

window.onload = onmodify